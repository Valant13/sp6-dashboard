<?php

namespace App\Service;

use App\Config;
use App\Entity\Dashboard\ConstructionData;
use App\Entity\Dashboard\DemandData;
use App\Entity\Dashboard\MaterialPriceData;
use App\Entity\Dashboard\MeetingDemandData;
use App\Entity\Dashboard\ProfitData;
use App\Entity\Dashboard\SaleData;
use App\Repository\Construction\ConstructionRepository;
use App\Repository\Construction\LotInvoiceRepository;
use App\Repository\Construction\MaterialInvoiceRepository;
use App\Repository\Sale\PeriodRepository;
use App\Repository\Sale\SaleRepository;

class DashboardService
{
    /**
     * @var ConstructionRepository
     */
    private $constructionRepository;

    /**
     * @var SaleRepository
     */
    private $saleRepository;

    /**
     * @var PeriodService
     */
    private $periodService;

    /**
     * @var MaterialInvoiceRepository
     */
    private $materialInvoiceRepository;

    /**
     * @var PeriodRepository
     */
    private $periodRepository;

    /**
     * @var LotInvoiceRepository
     */
    private $lotInvoiceRepository;

    /**
     * @param ConstructionRepository $constructionRepository
     * @param SaleRepository $saleRepository
     * @param PeriodService $periodService
     * @param MaterialInvoiceRepository $materialInvoiceRepository
     * @param PeriodRepository $periodRepository
     * @param LotInvoiceRepository $lotInvoiceRepository
     */
    public function __construct(
        ConstructionRepository $constructionRepository,
        SaleRepository $saleRepository,
        PeriodService $periodService,
        MaterialInvoiceRepository $materialInvoiceRepository,
        PeriodRepository $periodRepository,
        LotInvoiceRepository $lotInvoiceRepository
    ) {
        $this->constructionRepository = $constructionRepository;
        $this->saleRepository = $saleRepository;
        $this->periodService = $periodService;
        $this->materialInvoiceRepository = $materialInvoiceRepository;
        $this->periodRepository = $periodRepository;
        $this->lotInvoiceRepository = $lotInvoiceRepository;
    }

    /**
     * @return ConstructionData
     */
    public function getConstructionData(): ConstructionData
    {
        $areas = [
            'high_rise' => [],
            'mid_rise' => [],
            'low_rise' => []
        ];

        $constructions = $this->constructionRepository->findAll();
        foreach ($constructions as $construction) {
            $year = $construction->getDate();
            $buildingType = $construction->getProject()->getBuildingType();
            $area = $construction->getProject()->getTotalArea();

            if (!array_key_exists($year, $areas[$buildingType])) {
                $areas[$buildingType][$year] = 0;
            }

            $areas[$buildingType][$year] += $area;
        }

        $constructionData = new ConstructionData();

        $constructionData->setLabels($this->createLabels());
        $constructionData->setHighRiseDataset($this->createDataset($areas['high_rise']));
        $constructionData->setMidRiseDataset($this->createDataset($areas['mid_rise']));
        $constructionData->setLowRiseDataset($this->createDataset($areas['low_rise']));

        return $constructionData;
    }

    /**
     * @return MaterialPriceData
     */
    public function getMaterialPriceData(): MaterialPriceData
    {
        $prices = [
            'steel' => [],
            'concrete' => [],
            'bricks' => []
        ];

        $materialInvoices = $this->materialInvoiceRepository->findAll();
        foreach ($materialInvoices as $materialInvoice) {
            $year = $materialInvoice->getDate();

            $prices['steel'][$year] = $materialInvoice->getSteelPrice();
            $prices['concrete'][$year] = $materialInvoice->getConcretePrice();
            $prices['bricks'][$year] = $materialInvoice->getBricksPrice();
        }

        $materialPriceData = new MaterialPriceData();

        $materialPriceData->setLabels($this->createLabels());
        $materialPriceData->setSteelDataset($this->createDataset($prices['steel']));
        $materialPriceData->setConcreteDataset($this->createDataset($prices['concrete']));
        $materialPriceData->setBricksDataset($this->createDataset($prices['bricks']));

        return $materialPriceData;
    }

    /**
     * @return SaleData
     */
    public function getSaleData(): SaleData
    {
        $areas = [
            'one_bedroom' => 0,
            'two_bedroom' => 0,
            'three_bedroom' => 0
        ];

        $currentYear = $this->periodService->getCurrentYear();

        $sales = $this->saleRepository->findAll();
        foreach ($sales as $sale) {
            if ($sale->getDate() === $currentYear) {
                $apartmentType = $sale->getApartment()->getApartmentType();
                $area = $sale->getApartment()->getArea();

                $areas[$apartmentType] += $area;
            }
        }

        $saleData = new SaleData();

        $saleData->setDataset([$areas['one_bedroom'], $areas['two_bedroom'], $areas['three_bedroom']]);
        $saleData->setYear($currentYear);

        return $saleData;
    }

    /**
     * @return MeetingDemandData
     */
    public function getMeetingDemandData(): MeetingDemandData
    {
        $soldArea = 0;
        $totalArea = 0;

        $currentYear = $this->periodService->getCurrentYear();

        $periods = $this->periodRepository->findAll();
        foreach ($periods as $period) {
            if ($period->getYear() === $currentYear) {
                $apartmentArea = 0;
                switch ($period->getApartmentType()) {
                    case 'one_bedroom':
                        $apartmentArea = Config::ONE_BEDROOM_APARTMENT_AREA;
                        break;
                    case 'two_bedroom':
                        $apartmentArea = Config::TWO_BEDROOM_APARTMENT_AREA;
                        break;
                    case 'three_bedroom':
                        $apartmentArea = Config::THREE_BEDROOM_APARTMENT_AREA;
                        break;
                }

                $totalArea += $period->getDemand() * $apartmentArea;
            }
        }

        $sales = $this->saleRepository->findAll();
        foreach ($sales as $sale) {
            if ($sale->getDate() === $currentYear) {
                $soldArea += $sale->getApartment()->getArea();
            }
        }

        $soldPercentage = (int)($soldArea / $totalArea * 100);
        $unsoldPercentage = 100 - $soldPercentage;

        $meetingDemandData = new MeetingDemandData();

        $meetingDemandData->setDataset([$soldPercentage, $unsoldPercentage]);
        $meetingDemandData->setYear($currentYear);

        return $meetingDemandData;
    }

    /**
     * @return ProfitData
     */
    public function getProfitData(): ProfitData
    {
        $profit = [];

        $materialInvoices = $this->materialInvoiceRepository->findAll();
        foreach ($materialInvoices as $materialInvoice) {
            $year = $materialInvoice->getDate();

            if (!array_key_exists($year, $profit)) {
                $profit[$year] = 0;
            }

            $profit[$year] -= $materialInvoice->getTotal();
        }

        $lotInvoices = $this->lotInvoiceRepository->findAll();
        foreach ($lotInvoices as $lotInvoice) {
            $year = $lotInvoice->getDate();

            if (!array_key_exists($year, $profit)) {
                $profit[$year] = 0;
            }

            $profit[$year] -= $lotInvoice->getTotal();
        }

        $sales = $this->saleRepository->findAll();
        foreach ($sales as $sale) {
            $year = $sale->getDate();

            if (!array_key_exists($year, $profit)) {
                $profit[$year] = 0;
            }

            $profit[$year] += $sale->getTotal();
        }

        $profitData = new ProfitData();

        $profitData->setLabels($this->createLabels());
        $profitData->setDataset($this->createDataset($profit));

        return $profitData;
    }

    /**
     * @return DemandData
     */
    public function getDemandData(): DemandData
    {
        $demand = [
            'high_rise' => [
                'one_bedroom' => [],
                'two_bedroom' => [],
                'three_bedroom' => []
            ],
            'mid_rise' => [
                'one_bedroom' => [],
                'two_bedroom' => [],
                'three_bedroom' => []
            ],
            'low_rise' => [
                'one_bedroom' => [],
                'two_bedroom' => [],
                'three_bedroom' => []
            ]
        ];

        $periods = $this->periodRepository->findAll();
        foreach ($periods as $period) {
            $year = $period->getYear();
            $apartmentType = $period->getApartmentType();
            $buildingType = $period->getBuildingType();

            $demand[$buildingType][$apartmentType][$year] = $period->getDemand();
        }

        $demandData = new DemandData();

        $demandData->setLabels($this->createLabels());
        $demandData->setHighRiseOneBedroomDataset($this->createDataset($demand['high_rise']['one_bedroom']));
        $demandData->setHighRiseTwoBedroomDataset($this->createDataset($demand['high_rise']['two_bedroom']));
        $demandData->setHighRiseThreeBedroomDataset($this->createDataset($demand['high_rise']['three_bedroom']));
        $demandData->setMidRiseOneBedroomDataset($this->createDataset($demand['mid_rise']['one_bedroom']));
        $demandData->setMidRiseTwoBedroomDataset($this->createDataset($demand['mid_rise']['two_bedroom']));
        $demandData->setMidRiseThreeBedroomDataset($this->createDataset($demand['mid_rise']['three_bedroom']));
        $demandData->setLowRiseOneBedroomDataset($this->createDataset($demand['low_rise']['one_bedroom']));
        $demandData->setLowRiseTwoBedroomDataset($this->createDataset($demand['low_rise']['two_bedroom']));
        $demandData->setLowRiseThreeBedroomDataset($this->createDataset($demand['low_rise']['three_bedroom']));

        return $demandData;
    }

    /**
     * @return string[]
     */
    private function createLabels(): array
    {
        $currentYear = $this->periodService->getCurrentYear();

        $labels = [];
        for ($year = Config::STARTING_YEAR; $year <= $currentYear; $year++) {
            $labels[] = (string)$year;
        }

        return $labels;
    }

    /**
     * @param int[] $indexedValues
     * @return int[]
     */
    private function createDataset(array $indexedValues): array
    {
        $currentYear = $this->periodService->getCurrentYear();

        $dataset = [];
        for ($year = Config::STARTING_YEAR; $year <= $currentYear; $year++) {
            if (!array_key_exists($year, $indexedValues)) {
                $dataset[] = 0;
            } else {
                $dataset[] = $indexedValues[$year];
            }
        }

        return $dataset;
    }
}
