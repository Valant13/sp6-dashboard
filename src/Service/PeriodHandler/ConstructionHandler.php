<?php

namespace App\Service\PeriodHandler;

use App\Repository\Construction\ConstructionRepository;
use App\Repository\Construction\LotInvoiceRepository;
use App\Repository\Construction\MaterialInvoiceRepository;
use App\Repository\Construction\ProjectRepository;
use App\Service\PeriodHandlerInterface;
use Doctrine\ORM\EntityManagerInterface;

class ConstructionHandler implements PeriodHandlerInterface
{
    /**
     * @var ConstructionRepository
     */
    private $constructionRepository;

    /**
     * @var LotInvoiceRepository
     */
    private $lotInvoiceRepository;

    /**
     * @var MaterialInvoiceRepository
     */
    private $materialInvoiceRepository;

    /**
     * @var ProjectRepository
     */
    private $projectRepository;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @param ConstructionRepository $constructionRepository
     * @param LotInvoiceRepository $lotInvoiceRepository
     * @param MaterialInvoiceRepository $materialInvoiceRepository
     * @param ProjectRepository $projectRepository
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(
        ConstructionRepository $constructionRepository,
        LotInvoiceRepository $lotInvoiceRepository,
        MaterialInvoiceRepository $materialInvoiceRepository,
        ProjectRepository $projectRepository,
        EntityManagerInterface $entityManager
    ) {
        $this->constructionRepository = $constructionRepository;
        $this->lotInvoiceRepository = $lotInvoiceRepository;
        $this->materialInvoiceRepository = $materialInvoiceRepository;
        $this->projectRepository = $projectRepository;
        $this->entityManager = $entityManager;
    }

    public function reset(): void
    {
        $constructions = $this->constructionRepository->findAll();
        foreach ($constructions as $construction) {
            $this->entityManager->remove($construction);
        }

        $lotInvoices = $this->lotInvoiceRepository->findAll();
        foreach ($lotInvoices as $lotInvoice) {
            $this->entityManager->remove($lotInvoice);
        }

        $materialInvoices = $this->materialInvoiceRepository->findAll();
        foreach ($materialInvoices as $materialInvoice) {
            $this->entityManager->remove($materialInvoice);
        }

        $projects = $this->projectRepository->findAll();
        foreach ($projects as $project) {
            $this->entityManager->remove($project);
        }

        $this->entityManager->flush();
    }
}
