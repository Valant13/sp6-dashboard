<?php

namespace App\Service\PeriodHandler;

use App\Repository\Sale\ApartmentRepository;
use App\Repository\Sale\BuildingRepository;
use App\Repository\Sale\CustomerRepository;
use App\Repository\Sale\PeriodRepository;
use App\Repository\Sale\SaleRepository;
use App\Service\PeriodHandlerInterface;
use Doctrine\ORM\EntityManagerInterface;

class SaleHandler implements PeriodHandlerInterface
{
    /**
     * @var ApartmentRepository
     */
    private $apartmentRepository;

    /**
     * @var BuildingRepository
     */
    private $buildingRepository;

    /**
     * @var CustomerRepository
     */
    private $customerRepository;

    /**
     * @var PeriodRepository
     */
    private $periodRepository;

    /**
     * @var SaleRepository
     */
    private $saleRepository;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @param ApartmentRepository $apartmentRepository
     * @param BuildingRepository $buildingRepository
     * @param CustomerRepository $customerRepository
     * @param PeriodRepository $periodRepository
     * @param SaleRepository $saleRepository
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(
        ApartmentRepository $apartmentRepository,
        BuildingRepository $buildingRepository,
        CustomerRepository $customerRepository,
        PeriodRepository $periodRepository,
        SaleRepository $saleRepository,
        EntityManagerInterface $entityManager
    ) {
        $this->apartmentRepository = $apartmentRepository;
        $this->buildingRepository = $buildingRepository;
        $this->customerRepository = $customerRepository;
        $this->periodRepository = $periodRepository;
        $this->saleRepository = $saleRepository;
        $this->entityManager = $entityManager;
    }

    public function reset(): void
    {
        $sales = $this->saleRepository->findAll();
        foreach ($sales as $sale) {
            $this->entityManager->remove($sale);
        }

        $apartments = $this->apartmentRepository->findAll();
        foreach ($apartments as $apartment) {
            $this->entityManager->remove($apartment);
        }

        $buildings = $this->buildingRepository->findAll();
        foreach ($buildings as $building) {
            $this->entityManager->remove($building);
        }

        $customers = $this->customerRepository->findAll();
        foreach ($customers as $customer) {
            $this->entityManager->remove($customer);
        }

        $periods = $this->periodRepository->findAll();
        foreach ($periods as $period) {
            $this->entityManager->remove($period);
        }

        $this->entityManager->flush();
    }
}
