<?php

namespace App\Service;

use App\Entity\Period;
use App\Repository\PeriodRepository;
use Doctrine\ORM\EntityManagerInterface;

class PeriodService
{
    /**
     * @var PeriodRepository
     */
    private $periodRepository;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var PeriodHandlerInterface[]
     */
    private $handlers;

    /**
     * @param PeriodRepository $periodRepository
     * @param EntityManagerInterface $entityManager
     * @param PeriodHandlerInterface[] $handlers
     */
    public function __construct(
        PeriodRepository $periodRepository,
        EntityManagerInterface $entityManager,
        array $handlers
    ) {
        $this->periodRepository = $periodRepository;
        $this->entityManager = $entityManager;
        $this->handlers = $handlers;
    }

    /**
     * @return int
     */
    public function getCurrentYear(): int
    {
        return $this->periodRepository->findOne()->getYear() - 1;
    }

    /**
     * @param int $currentYear
     */
    public function setCurrentYear(int $currentYear): void
    {
        $period = $this->periodRepository->findOne();

        if ($period === null) {
            $period = new Period();
            $this->entityManager->persist($period);
        }

        $period->setYear($currentYear);
        $this->entityManager->flush();

        $this->resetAppState();
    }

    /**
     *
     */
    private function resetAppState(): void
    {
        foreach ($this->handlers as $handler) {
            $handler->reset();
        }
    }
}
