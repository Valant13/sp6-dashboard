<?php

namespace App\Entity\Dashboard;

class SaleData
{
    /**
     * @var int[]
     */
    private $dataset;

    /**
     * @var int
     */
    private $year;

    /**
     * @return int[]
     */
    public function getDataset(): array
    {
        return $this->dataset;
    }

    /**
     * @param int[] $dataset
     */
    public function setDataset(array $dataset): void
    {
        $this->dataset = $dataset;
    }

    /**
     * @return int
     */
    public function getYear(): int
    {
        return $this->year;
    }

    /**
     * @param int $year
     */
    public function setYear(int $year): void
    {
        $this->year = $year;
    }
}
