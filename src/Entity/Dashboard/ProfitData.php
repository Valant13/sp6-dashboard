<?php

namespace App\Entity\Dashboard;

class ProfitData
{
    /**
     * @var string[]
     */
    private $labels;

    /**
     * @var int[]
     */
    private $dataset;

    /**
     * @return string[]
     */
    public function getLabels(): array
    {
        return $this->labels;
    }

    /**
     * @param string[] $labels
     */
    public function setLabels(array $labels): void
    {
        $this->labels = $labels;
    }

    /**
     * @return int[]
     */
    public function getDataset(): array
    {
        return $this->dataset;
    }

    /**
     * @param int[] $dataset
     */
    public function setDataset(array $dataset): void
    {
        $this->dataset = $dataset;
    }
}
