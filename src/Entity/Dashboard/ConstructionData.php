<?php

namespace App\Entity\Dashboard;

class ConstructionData
{
    /**
     * @var string[]
     */
    private $labels;

    /**
     * @var int[]
     */
    private $highRiseDataset;

    /**
     * @var int[]
     */
    private $midRiseDataset;

    /**
     * @var int[]
     */
    private $lowRiseDataset;

    /**
     * @return string[]
     */
    public function getLabels(): array
    {
        return $this->labels;
    }

    /**
     * @param string[] $labels
     */
    public function setLabels(array $labels): void
    {
        $this->labels = $labels;
    }

    /**
     * @return int[]
     */
    public function getHighRiseDataset(): array
    {
        return $this->highRiseDataset;
    }

    /**
     * @param int[] $highRiseDataset
     */
    public function setHighRiseDataset(array $highRiseDataset): void
    {
        $this->highRiseDataset = $highRiseDataset;
    }

    /**
     * @return int[]
     */
    public function getMidRiseDataset(): array
    {
        return $this->midRiseDataset;
    }

    /**
     * @param int[] $midRiseDataset
     */
    public function setMidRiseDataset(array $midRiseDataset): void
    {
        $this->midRiseDataset = $midRiseDataset;
    }

    /**
     * @return int[]
     */
    public function getLowRiseDataset(): array
    {
        return $this->lowRiseDataset;
    }

    /**
     * @param int[] $lowRiseDataset
     */
    public function setLowRiseDataset(array $lowRiseDataset): void
    {
        $this->lowRiseDataset = $lowRiseDataset;
    }
}
