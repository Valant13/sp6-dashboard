<?php

namespace App\Entity\Dashboard;

class DemandData
{
    /**
     * @var string[]
     */
    private $labels;

    /**
     * @var int[]
     */
    private $highRiseOneBedroomDataset;

    /**
     * @var int[]
     */
    private $highRiseTwoBedroomDataset;

    /**
     * @var int[]
     */
    private $highRiseThreeBedroomDataset;

    /**
     * @var int[]
     */
    private $midRiseOneBedroomDataset;

    /**
     * @var int[]
     */
    private $midRiseTwoBedroomDataset;

    /**
     * @var int[]
     */
    private $midRiseThreeBedroomDataset;

    /**
     * @var int[]
     */
    private $lowRiseOneBedroomDataset;

    /**
     * @var int[]
     */
    private $lowRiseTwoBedroomDataset;

    /**
     * @var int[]
     */
    private $lowRiseThreeBedroomDataset;

    /**
     * @return string[]
     */
    public function getLabels(): array
    {
        return $this->labels;
    }

    /**
     * @param string[] $labels
     */
    public function setLabels(array $labels): void
    {
        $this->labels = $labels;
    }

    /**
     * @return int[]
     */
    public function getHighRiseOneBedroomDataset(): array
    {
        return $this->highRiseOneBedroomDataset;
    }

    /**
     * @param int[] $highRiseOneBedroomDataset
     */
    public function setHighRiseOneBedroomDataset(array $highRiseOneBedroomDataset): void
    {
        $this->highRiseOneBedroomDataset = $highRiseOneBedroomDataset;
    }

    /**
     * @return int[]
     */
    public function getHighRiseTwoBedroomDataset(): array
    {
        return $this->highRiseTwoBedroomDataset;
    }

    /**
     * @param int[] $highRiseTwoBedroomDataset
     */
    public function setHighRiseTwoBedroomDataset(array $highRiseTwoBedroomDataset): void
    {
        $this->highRiseTwoBedroomDataset = $highRiseTwoBedroomDataset;
    }

    /**
     * @return int[]
     */
    public function getHighRiseThreeBedroomDataset(): array
    {
        return $this->highRiseThreeBedroomDataset;
    }

    /**
     * @param int[] $highRiseThreeBedroomDataset
     */
    public function setHighRiseThreeBedroomDataset(array $highRiseThreeBedroomDataset): void
    {
        $this->highRiseThreeBedroomDataset = $highRiseThreeBedroomDataset;
    }

    /**
     * @return int[]
     */
    public function getMidRiseOneBedroomDataset(): array
    {
        return $this->midRiseOneBedroomDataset;
    }

    /**
     * @param int[] $midRiseOneBedroomDataset
     */
    public function setMidRiseOneBedroomDataset(array $midRiseOneBedroomDataset): void
    {
        $this->midRiseOneBedroomDataset = $midRiseOneBedroomDataset;
    }

    /**
     * @return int[]
     */
    public function getMidRiseTwoBedroomDataset(): array
    {
        return $this->midRiseTwoBedroomDataset;
    }

    /**
     * @param int[] $midRiseTwoBedroomDataset
     */
    public function setMidRiseTwoBedroomDataset(array $midRiseTwoBedroomDataset): void
    {
        $this->midRiseTwoBedroomDataset = $midRiseTwoBedroomDataset;
    }

    /**
     * @return int[]
     */
    public function getMidRiseThreeBedroomDataset(): array
    {
        return $this->midRiseThreeBedroomDataset;
    }

    /**
     * @param int[] $midRiseThreeBedroomDataset
     */
    public function setMidRiseThreeBedroomDataset(array $midRiseThreeBedroomDataset): void
    {
        $this->midRiseThreeBedroomDataset = $midRiseThreeBedroomDataset;
    }

    /**
     * @return int[]
     */
    public function getLowRiseOneBedroomDataset(): array
    {
        return $this->lowRiseOneBedroomDataset;
    }

    /**
     * @param int[] $lowRiseOneBedroomDataset
     */
    public function setLowRiseOneBedroomDataset(array $lowRiseOneBedroomDataset): void
    {
        $this->lowRiseOneBedroomDataset = $lowRiseOneBedroomDataset;
    }

    /**
     * @return int[]
     */
    public function getLowRiseTwoBedroomDataset(): array
    {
        return $this->lowRiseTwoBedroomDataset;
    }

    /**
     * @param int[] $lowRiseTwoBedroomDataset
     */
    public function setLowRiseTwoBedroomDataset(array $lowRiseTwoBedroomDataset): void
    {
        $this->lowRiseTwoBedroomDataset = $lowRiseTwoBedroomDataset;
    }

    /**
     * @return int[]
     */
    public function getLowRiseThreeBedroomDataset(): array
    {
        return $this->lowRiseThreeBedroomDataset;
    }

    /**
     * @param int[] $lowRiseThreeBedroomDataset
     */
    public function setLowRiseThreeBedroomDataset(array $lowRiseThreeBedroomDataset): void
    {
        $this->lowRiseThreeBedroomDataset = $lowRiseThreeBedroomDataset;
    }
}
