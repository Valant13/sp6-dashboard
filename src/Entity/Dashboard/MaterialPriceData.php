<?php

namespace App\Entity\Dashboard;

class MaterialPriceData
{
    /**
     * @var string[]
     */
    private $labels;

    /**
     * @var int[]
     */
    private $steelDataset;

    /**
     * @var int[]
     */
    private $concreteDataset;

    /**
     * @var int[]
     */
    private $bricksDataset;

    /**
     * @return string[]
     */
    public function getLabels(): array
    {
        return $this->labels;
    }

    /**
     * @param string[] $labels
     */
    public function setLabels(array $labels): void
    {
        $this->labels = $labels;
    }

    /**
     * @return int[]
     */
    public function getSteelDataset(): array
    {
        return $this->steelDataset;
    }

    /**
     * @param int[] $steelDataset
     */
    public function setSteelDataset(array $steelDataset): void
    {
        $this->steelDataset = $steelDataset;
    }

    /**
     * @return int[]
     */
    public function getConcreteDataset(): array
    {
        return $this->concreteDataset;
    }

    /**
     * @param int[] $concreteDataset
     */
    public function setConcreteDataset(array $concreteDataset): void
    {
        $this->concreteDataset = $concreteDataset;
    }

    /**
     * @return int[]
     */
    public function getBricksDataset(): array
    {
        return $this->bricksDataset;
    }

    /**
     * @param int[] $bricksDataset
     */
    public function setBricksDataset(array $bricksDataset): void
    {
        $this->bricksDataset = $bricksDataset;
    }
}
