<?php

namespace App\Entity\Construction;

use App\Repository\Construction\ConstructionRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ConstructionRepository::class)
 */
class Construction
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Project::class, inversedBy="constructions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $project;

    /**
     * @ORM\ManyToOne(targetEntity=MaterialInvoice::class, inversedBy="constructions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $materialInvoice;

    /**
     * @ORM\ManyToOne(targetEntity=LotInvoice::class, inversedBy="constructions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $lotInvoice;

    /**
     * @ORM\Column(type="integer")
     */
    private $date;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProject(): ?Project
    {
        return $this->project;
    }

    public function setProject(?Project $project): self
    {
        $this->project = $project;

        return $this;
    }

    public function getMaterialInvoice(): ?MaterialInvoice
    {
        return $this->materialInvoice;
    }

    public function setMaterialInvoice(?MaterialInvoice $materialInvoice): self
    {
        $this->materialInvoice = $materialInvoice;

        return $this;
    }

    public function getLotInvoice(): ?LotInvoice
    {
        return $this->lotInvoice;
    }

    public function setLotInvoice(?LotInvoice $lotInvoice): self
    {
        $this->lotInvoice = $lotInvoice;

        return $this;
    }

    public function getDate(): ?int
    {
        return $this->date;
    }

    public function setDate(int $date): self
    {
        $this->date = $date;

        return $this;
    }
}
