<?php

namespace App\Entity\Construction;

use App\Repository\Construction\MaterialInvoiceRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=MaterialInvoiceRepository::class)
 */
class MaterialInvoice
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $steelPrice;

    /**
     * @ORM\Column(type="integer")
     */
    private $steelQty;

    /**
     * @ORM\Column(type="integer")
     */
    private $concretePrice;

    /**
     * @ORM\Column(type="integer")
     */
    private $concreteQty;

    /**
     * @ORM\Column(type="integer")
     */
    private $bricksPrice;

    /**
     * @ORM\Column(type="integer")
     */
    private $bricksQty;

    /**
     * @ORM\Column(type="integer")
     */
    private $date;

    /**
     * @ORM\Column(type="integer")
     */
    private $total;

    /**
     * @ORM\OneToMany(targetEntity=Construction::class, mappedBy="materialInvoice")
     */
    private $constructions;

    public function __construct()
    {
        $this->constructions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSteelPrice(): ?int
    {
        return $this->steelPrice;
    }

    public function setSteelPrice(int $steelPrice): self
    {
        $this->steelPrice = $steelPrice;

        return $this;
    }

    public function getSteelQty(): ?int
    {
        return $this->steelQty;
    }

    public function setSteelQty(int $steelQty): self
    {
        $this->steelQty = $steelQty;

        return $this;
    }

    public function getConcretePrice(): ?int
    {
        return $this->concretePrice;
    }

    public function setConcretePrice(int $concretePrice): self
    {
        $this->concretePrice = $concretePrice;

        return $this;
    }

    public function getConcreteQty(): ?int
    {
        return $this->concreteQty;
    }

    public function setConcreteQty(int $concreteQty): self
    {
        $this->concreteQty = $concreteQty;

        return $this;
    }

    public function getBricksPrice(): ?int
    {
        return $this->bricksPrice;
    }

    public function setBricksPrice(int $bricksPrice): self
    {
        $this->bricksPrice = $bricksPrice;

        return $this;
    }

    public function getBricksQty(): ?int
    {
        return $this->bricksQty;
    }

    public function setBricksQty(int $bricksQty): self
    {
        $this->bricksQty = $bricksQty;

        return $this;
    }

    public function getDate(): ?int
    {
        return $this->date;
    }

    public function setDate(int $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getTotal(): ?int
    {
        return $this->total;
    }

    public function setTotal(int $total): self
    {
        $this->total = $total;

        return $this;
    }

    /**
     * @return Collection|Construction[]
     */
    public function getConstructions(): Collection
    {
        return $this->constructions;
    }

    public function addConstruction(Construction $construction): self
    {
        if (!$this->constructions->contains($construction)) {
            $this->constructions[] = $construction;
            $construction->setMaterialInvoice($this);
        }

        return $this;
    }

    public function removeConstruction(Construction $construction): self
    {
        if ($this->constructions->removeElement($construction)) {
            // set the owning side to null (unless already changed)
            if ($construction->getMaterialInvoice() === $this) {
                $construction->setMaterialInvoice(null);
            }
        }

        return $this;
    }
}
