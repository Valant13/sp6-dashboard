<?php

namespace App\Entity\Construction;

use App\Repository\Construction\ProjectRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ProjectRepository::class)
 */
class Project
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $buildingType;

    /**
     * @ORM\Column(type="integer")
     */
    private $totalArea;

    /**
     * @ORM\Column(type="integer")
     */
    private $steelNeed;

    /**
     * @ORM\Column(type="integer")
     */
    private $concreteNeed;

    /**
     * @ORM\Column(type="integer")
     */
    private $bricksNeed;

    /**
     * @ORM\Column(type="integer")
     */
    private $lotAreaNeed;

    /**
     * @ORM\Column(type="integer")
     */
    private $developmentDate;

    /**
     * @ORM\OneToMany(targetEntity=Construction::class, mappedBy="project")
     */
    private $constructions;

    public function __construct()
    {
        $this->constructions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBuildingType(): ?string
    {
        return $this->buildingType;
    }

    public function setBuildingType(string $buildingType): self
    {
        $this->buildingType = $buildingType;

        return $this;
    }

    public function getTotalArea(): ?int
    {
        return $this->totalArea;
    }

    public function setTotalArea(int $totalArea): self
    {
        $this->totalArea = $totalArea;

        return $this;
    }

    public function getSteelNeed(): ?int
    {
        return $this->steelNeed;
    }

    public function setSteelNeed(int $steelNeed): self
    {
        $this->steelNeed = $steelNeed;

        return $this;
    }

    public function getConcreteNeed(): ?int
    {
        return $this->concreteNeed;
    }

    public function setConcreteNeed(int $concreteNeed): self
    {
        $this->concreteNeed = $concreteNeed;

        return $this;
    }

    public function getBricksNeed(): ?int
    {
        return $this->bricksNeed;
    }

    public function setBricksNeed(int $bricksNeed): self
    {
        $this->bricksNeed = $bricksNeed;

        return $this;
    }

    public function getLotAreaNeed(): ?int
    {
        return $this->lotAreaNeed;
    }

    public function setLotAreaNeed(int $lotAreaNeed): self
    {
        $this->lotAreaNeed = $lotAreaNeed;

        return $this;
    }

    public function getDevelopmentDate(): ?int
    {
        return $this->developmentDate;
    }

    public function setDevelopmentDate(int $developmentDate): self
    {
        $this->developmentDate = $developmentDate;

        return $this;
    }

    /**
     * @return Collection|Construction[]
     */
    public function getConstructions(): Collection
    {
        return $this->constructions;
    }

    public function addConstruction(Construction $construction): self
    {
        if (!$this->constructions->contains($construction)) {
            $this->constructions[] = $construction;
            $construction->setProject($this);
        }

        return $this;
    }

    public function removeConstruction(Construction $construction): self
    {
        if ($this->constructions->removeElement($construction)) {
            // set the owning side to null (unless already changed)
            if ($construction->getProject() === $this) {
                $construction->setProject(null);
            }
        }

        return $this;
    }
}
