<?php

namespace App\Entity\Sale;

use App\Repository\Sale\BuildingRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=BuildingRepository::class)
 */
class Building
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $buildingType;

    /**
     * @ORM\Column(type="integer")
     */
    private $totalArea;

    /**
     * @ORM\Column(type="integer")
     */
    private $constructionDate;

    /**
     * @ORM\Column(type="integer")
     */
    private $constructionCost;

    /**
     * @ORM\OneToMany(targetEntity=Sale::class, mappedBy="building")
     */
    private $sales;

    public function __construct()
    {
        $this->sales = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBuildingType(): ?string
    {
        return $this->buildingType;
    }

    public function setBuildingType(string $buildingType): self
    {
        $this->buildingType = $buildingType;

        return $this;
    }

    public function getTotalArea(): ?int
    {
        return $this->totalArea;
    }

    public function setTotalArea(int $totalArea): self
    {
        $this->totalArea = $totalArea;

        return $this;
    }

    public function getConstructionDate(): ?int
    {
        return $this->constructionDate;
    }

    public function setConstructionDate(int $constructionDate): self
    {
        $this->constructionDate = $constructionDate;

        return $this;
    }

    public function getConstructionCost(): ?int
    {
        return $this->constructionCost;
    }

    public function setConstructionCost(int $constructionCost): self
    {
        $this->constructionCost = $constructionCost;

        return $this;
    }

    /**
     * @return Collection|Sale[]
     */
    public function getSales(): Collection
    {
        return $this->sales;
    }

    public function addSale(Sale $sale): self
    {
        if (!$this->sales->contains($sale)) {
            $this->sales[] = $sale;
            $sale->setBuilding($this);
        }

        return $this;
    }

    public function removeSale(Sale $sale): self
    {
        if ($this->sales->removeElement($sale)) {
            // set the owning side to null (unless already changed)
            if ($sale->getBuilding() === $this) {
                $sale->setBuilding(null);
            }
        }

        return $this;
    }
}
