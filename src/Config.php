<?php

namespace App;

class Config
{
    const STARTING_YEAR = 1992;

    const ONE_BEDROOM_APARTMENT_AREA = 25;
    const TWO_BEDROOM_APARTMENT_AREA = 50;
    const THREE_BEDROOM_APARTMENT_AREA = 75;
}
