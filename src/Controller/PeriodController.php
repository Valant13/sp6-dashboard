<?php

namespace App\Controller;

use App\Service\PeriodService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PeriodController extends AbstractController
{
    /**
     * @var PeriodService
     */
    private $periodService;

    /**
     * @param PeriodService $periodService
     */
    public function __construct(PeriodService $periodService)
    {
        $this->periodService = $periodService;
    }

    /**
     * @Route("/period/year/{year}", methods="POST", name="period_year")
     */
    public function year(int $year): Response
    {
        $this->periodService->setCurrentYear($year);

        return new Response();
    }
}
