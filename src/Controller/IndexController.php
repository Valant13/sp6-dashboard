<?php

namespace App\Controller;

use App\Service\DashboardService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends AbstractController
{
    /**
     * @var DashboardService
     */
    private $dashboardService;

    /**
     * @param DashboardService $dashboardService
     */
    public function __construct(DashboardService $dashboardService)
    {
        $this->dashboardService = $dashboardService;
    }
    /**
     * @Route("/", methods="GET", name="index")
     */
    public function index(): Response
    {
        $constructionData = $this->dashboardService->getConstructionData();
        $demandData = $this->dashboardService->getDemandData();
        $materialPriceData = $this->dashboardService->getMaterialPriceData();
        $meetingDemandData = $this->dashboardService->getMeetingDemandData();
        $profitData = $this->dashboardService->getProfitData();
        $saleData = $this->dashboardService->getSaleData();

        return $this->render('base.html.twig', [
            'constructionData' => $constructionData,
            'demandData' => $demandData,
            'materialPriceData' => $materialPriceData,
            'meetingDemandData' => $meetingDemandData,
            'profitData' => $profitData,
            'saleData' => $saleData
        ]);
    }
}
