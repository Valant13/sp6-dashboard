<?php

namespace App\Controller;

use App\Entity\Construction\Construction;
use App\Entity\Construction\LotInvoice;
use App\Entity\Construction\MaterialInvoice;
use App\Entity\Construction\Project;
use App\Repository\Construction\LotInvoiceRepository;
use App\Repository\Construction\MaterialInvoiceRepository;
use App\Repository\Construction\ProjectRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ConstructionController extends AbstractController
{
    /**
     * @var LotInvoiceRepository
     */
    private $lotInvoiceRepository;

    /**
     * @var MaterialInvoiceRepository
     */
    private $materialInvoiceRepository;

    /**
     * @var ProjectRepository
     */
    private $projectRepository;

    /**
     * @var Request
     */
    private $request;

    /**
     * @param LotInvoiceRepository $lotInvoiceRepository
     * @param MaterialInvoiceRepository $materialInvoiceRepository
     * @param ProjectRepository $projectRepository
     * @param RequestStack $requestStack
     */
    public function __construct(
        LotInvoiceRepository $lotInvoiceRepository,
        MaterialInvoiceRepository $materialInvoiceRepository,
        ProjectRepository $projectRepository,
        RequestStack $requestStack
    ) {
        $this->lotInvoiceRepository = $lotInvoiceRepository;
        $this->materialInvoiceRepository = $materialInvoiceRepository;
        $this->projectRepository = $projectRepository;
        $this->request = $requestStack->getCurrentRequest();
    }

    /**
     * @Route("/construction/constructions", methods="POST", name="construction_constructions")
     */
    public function constructions(): Response
    {
        $data = json_decode($this->request->getContent(), true);

        $constructions = [];
        foreach ($data as $dataItem) {
            $construction = new Construction();
            $construction->setProject($this->projectRepository->find($dataItem['project_id']));
            $construction->setMaterialInvoice($this->materialInvoiceRepository->find($dataItem['material_invoice_id']));
            $construction->setLotInvoice($this->lotInvoiceRepository->find($dataItem['lot_invoice_id']));
            $construction->setDate($dataItem['date']);

            $this->getDoctrine()->getManager()->persist($construction);

            $constructions[] = $construction;
        }

        $this->getDoctrine()->getManager()->flush();

        $ids = [];
        foreach ($constructions as $construction) {
            $ids[] = $construction->getId();
        }

        return $this->json($ids);
    }

    /**
     * @Route("/construction/lot-invoices", methods="POST", name="construction_lot_invoices")
     */
    public function lotInvoices(): Response
    {
        $data = json_decode($this->request->getContent(), true);

        $lotInvoices = [];
        foreach ($data as $dataItem) {
            $lotInvoice = new LotInvoice();
            $lotInvoice->setLotArea($dataItem['lot_area']);
            $lotInvoice->setDate($dataItem['date']);
            $lotInvoice->setTotal($dataItem['total']);

            $this->getDoctrine()->getManager()->persist($lotInvoice);

            $lotInvoices[] = $lotInvoice;
        }

        $this->getDoctrine()->getManager()->flush();

        $ids = [];
        foreach ($lotInvoices as $lotInvoice) {
            $ids[] = $lotInvoice->getId();
        }

        return $this->json($ids);
    }

    /**
     * @Route("/construction/material-invoices", methods="POST", name="construction_material_invoices")
     */
    public function materialInvoices(): Response
    {
        $data = json_decode($this->request->getContent(), true);

        $materialInvoices = [];
        foreach ($data as $dataItem) {
            $materialInvoice = new MaterialInvoice();
            $materialInvoice->setSteelPrice($dataItem['steel_price']);
            $materialInvoice->setSteelQty($dataItem['steel_qty']);
            $materialInvoice->setConcretePrice($dataItem['concrete_price']);
            $materialInvoice->setConcreteQty($dataItem['concrete_qty']);
            $materialInvoice->setBricksPrice($dataItem['bricks_price']);
            $materialInvoice->setBricksQty($dataItem['bricks_qty']);
            $materialInvoice->setDate($dataItem['date']);
            $materialInvoice->setTotal($dataItem['total']);

            $this->getDoctrine()->getManager()->persist($materialInvoice);

            $materialInvoices[] = $materialInvoice;
        }

        $this->getDoctrine()->getManager()->flush();

        $ids = [];
        foreach ($materialInvoices as $materialInvoice) {
            $ids[] = $materialInvoice->getId();
        }

        return $this->json($ids);
    }

    /**
     * @Route("/construction/projects", methods="POST", name="construction_projects")
     */
    public function projects(): Response
    {
        $data = json_decode($this->request->getContent(), true);

        $projects = [];
        foreach ($data as $dataItem) {
            $project = new Project();
            $project->setBuildingType($dataItem['building_type']);
            $project->setTotalArea($dataItem['total_area']);
            $project->setSteelNeed($dataItem['steel_need']);
            $project->setConcreteNeed($dataItem['concrete_need']);
            $project->setBricksNeed($dataItem['bricks_need']);
            $project->setLotAreaNeed($dataItem['lot_area_need']);
            $project->setDevelopmentDate($dataItem['development_date']);

            $this->getDoctrine()->getManager()->persist($project);

            $projects[] = $project;
        }

        $this->getDoctrine()->getManager()->flush();

        $ids = [];
        foreach ($projects as $project) {
            $ids[] = $project->getId();
        }

        return $this->json($ids);
    }
}
