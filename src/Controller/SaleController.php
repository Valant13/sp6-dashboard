<?php

namespace App\Controller;

use App\Entity\Sale\Apartment;
use App\Entity\Sale\Building;
use App\Entity\Sale\Customer;
use App\Entity\Sale\Period;
use App\Entity\Sale\Sale;
use App\Repository\Sale\ApartmentRepository;
use App\Repository\Sale\BuildingRepository;
use App\Repository\Sale\CustomerRepository;
use App\Repository\Sale\PeriodRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SaleController extends AbstractController
{
    /**
     * @var ApartmentRepository
     */
    private $apartmentRepository;

    /**
     * @var BuildingRepository
     */
    private $buildingRepository;

    /**
     * @var CustomerRepository
     */
    private $customerRepository;

    /**
     * @var PeriodRepository
     */
    private $periodRepository;

    /**
     * @var Request
     */
    private $request;

    /**
     * @param ApartmentRepository $apartmentRepository
     * @param BuildingRepository $buildingRepository
     * @param CustomerRepository $customerRepository
     * @param PeriodRepository $periodRepository
     * @param RequestStack $requestStack
     */
    public function __construct(
        ApartmentRepository $apartmentRepository,
        BuildingRepository $buildingRepository,
        CustomerRepository $customerRepository,
        PeriodRepository $periodRepository,
        RequestStack $requestStack
    ) {
        $this->apartmentRepository = $apartmentRepository;
        $this->buildingRepository = $buildingRepository;
        $this->customerRepository = $customerRepository;
        $this->periodRepository = $periodRepository;
        $this->request = $requestStack->getCurrentRequest();
    }

    /**
     * @Route("/sale/apartments", methods="POST", name="sale_apartments")
     */
    public function apartments(): Response
    {
        $data = json_decode($this->request->getContent(), true);

        $apartments = [];
        foreach ($data as $dataItem) {
            $apartment = new Apartment();
            $apartment->setApartmentType($dataItem['apartment_type']);
            $apartment->setArea($dataItem['area']);

            $this->getDoctrine()->getManager()->persist($apartment);

            $apartments[] = $apartment;
        }

        $this->getDoctrine()->getManager()->flush();

        $ids = [];
        foreach ($apartments as $apartment) {
            $ids[] = $apartment->getId();
        }

        return $this->json($ids);
    }

    /**
     * @Route("/sale/buildings", methods="POST", name="sale_buildings")
     */
    public function buildings(): Response
    {
        $data = json_decode($this->request->getContent(), true);

        $buildings = [];
        foreach ($data as $dataItem) {
            $building = new Building();
            $building->setBuildingType($dataItem['building_type']);
            $building->setTotalArea($dataItem['total_area']);
            $building->setConstructionDate($dataItem['construction_date']);
            $building->setConstructionCost($dataItem['construction_cost']);

            $this->getDoctrine()->getManager()->persist($building);

            $buildings[] = $building;
        }

        $this->getDoctrine()->getManager()->flush();

        $ids = [];
        foreach ($buildings as $building) {
            $ids[] = $building->getId();
        }

        return $this->json($ids);
    }

    /**
     * @Route("/sale/customers", methods="POST", name="sale_customers")
     */
    public function customers(): Response
    {
        $data = json_decode($this->request->getContent(), true);

        $customers = [];
        foreach ($data as $dataItem) {
            $customer = new Customer();
            $customer->setFirstName($dataItem['first_name']);
            $customer->setLastName($dataItem['last_name']);

            $this->getDoctrine()->getManager()->persist($customer);

            $customers[] = $customer;
        }

        $this->getDoctrine()->getManager()->flush();

        $ids = [];
        foreach ($customers as $customer) {
            $ids[] = $customer->getId();
        }

        return $this->json($ids);
    }

    /**
     * @Route("/sale/periods", methods="POST", name="sale_periods")
     */
    public function periods(): Response
    {
        $data = json_decode($this->request->getContent(), true);

        $periods = [];
        foreach ($data as $dataItem) {
            $period = new Period();
            $period->setYear($dataItem['year']);
            $period->setApartmentType($dataItem['apartment_type']);
            $period->setBuildingType($dataItem['building_type']);
            $period->setDemand($dataItem['demand']);

            $this->getDoctrine()->getManager()->persist($period);

            $periods[] = $period;
        }

        $this->getDoctrine()->getManager()->flush();

        $ids = [];
        foreach ($periods as $period) {
            $ids[] = $period->getId();
        }

        return $this->json($ids);
    }

    /**
     * @Route("/sale/sales", methods="POST", name="sale_sales")
     */
    public function sales(): Response
    {
        $data = json_decode($this->request->getContent(), true);

        $sales = [];
        foreach ($data as $dataItem) {
            $sale = new Sale();
            $sale->setPeriod($this->periodRepository->find($dataItem['period_id']));
            $sale->setApartment($this->apartmentRepository->find($dataItem['apartment_id']));
            $sale->setBuilding($this->buildingRepository->find($dataItem['building_id']));
            $sale->setCustomer($this->customerRepository->find($dataItem['customer_id']));
            $sale->setDate($dataItem['date']);
            $sale->setTotal($dataItem['total']);

            $this->getDoctrine()->getManager()->persist($sale);

            $sales[] = $sale;
        }

        $this->getDoctrine()->getManager()->flush();

        $ids = [];
        foreach ($sales as $sale) {
            $ids[] = $sale->getId();
        }

        return $this->json($ids);
    }
}
