<?php

namespace App\Repository\Construction;

use App\Entity\Construction\MaterialInvoice;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method MaterialInvoice|null find($id, $lockMode = null, $lockVersion = null)
 * @method MaterialInvoice|null findOneBy(array $criteria, array $orderBy = null)
 * @method MaterialInvoice[]    findAll()
 * @method MaterialInvoice[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MaterialInvoiceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MaterialInvoice::class);
    }

    // /**
    //  * @return MaterialInvoice[] Returns an array of MaterialInvoice objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?MaterialInvoice
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
