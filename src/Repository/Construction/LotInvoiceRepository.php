<?php

namespace App\Repository\Construction;

use App\Entity\Construction\LotInvoice;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method LotInvoice|null find($id, $lockMode = null, $lockVersion = null)
 * @method LotInvoice|null findOneBy(array $criteria, array $orderBy = null)
 * @method LotInvoice[]    findAll()
 * @method LotInvoice[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LotInvoiceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LotInvoice::class);
    }

    // /**
    //  * @return LotInvoice[] Returns an array of LotInvoice objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?LotInvoice
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
