<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220118225913 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE apartment (id INT AUTO_INCREMENT NOT NULL, apartment_type VARCHAR(255) NOT NULL, area INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE building (id INT AUTO_INCREMENT NOT NULL, building_type VARCHAR(255) NOT NULL, total_area INT NOT NULL, construction_date INT NOT NULL, construction_cost INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE construction (id INT AUTO_INCREMENT NOT NULL, project_id INT NOT NULL, material_invoice_id INT NOT NULL, lot_invoice_id INT NOT NULL, date INT NOT NULL, INDEX IDX_DC91E26E166D1F9C (project_id), INDEX IDX_DC91E26E89BB8D9F (material_invoice_id), INDEX IDX_DC91E26E5819624A (lot_invoice_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE customer (id INT AUTO_INCREMENT NOT NULL, first_name VARCHAR(255) NOT NULL, last_name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE lot_invoice (id INT AUTO_INCREMENT NOT NULL, lot_area INT NOT NULL, date INT NOT NULL, total INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE material_invoice (id INT AUTO_INCREMENT NOT NULL, steel_price INT NOT NULL, steel_qty INT NOT NULL, concrete_price INT NOT NULL, concrete_qty INT NOT NULL, bricks_price INT NOT NULL, bricks_qty INT NOT NULL, date INT NOT NULL, total INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE period (id INT AUTO_INCREMENT NOT NULL, year INT NOT NULL, apartment_type VARCHAR(255) NOT NULL, building_type VARCHAR(255) NOT NULL, demand INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE project (id INT AUTO_INCREMENT NOT NULL, building_type VARCHAR(255) NOT NULL, total_area INT NOT NULL, steel_need INT NOT NULL, concrete_need INT NOT NULL, bricks_need INT NOT NULL, lot_area_need INT NOT NULL, development_date INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE sale (id INT AUTO_INCREMENT NOT NULL, period_id INT NOT NULL, apartment_id INT NOT NULL, building_id INT NOT NULL, customer_id INT NOT NULL, date INT NOT NULL, total INT NOT NULL, INDEX IDX_E54BC005EC8B7ADE (period_id), INDEX IDX_E54BC005176DFE85 (apartment_id), INDEX IDX_E54BC0054D2A7E12 (building_id), INDEX IDX_E54BC0059395C3F3 (customer_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE messenger_messages (id BIGINT AUTO_INCREMENT NOT NULL, body LONGTEXT NOT NULL, headers LONGTEXT NOT NULL, queue_name VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, available_at DATETIME NOT NULL, delivered_at DATETIME DEFAULT NULL, INDEX IDX_75EA56E016BA31DB (delivered_at), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE construction ADD CONSTRAINT FK_DC91E26E166D1F9C FOREIGN KEY (project_id) REFERENCES project (id)');
        $this->addSql('ALTER TABLE construction ADD CONSTRAINT FK_DC91E26E89BB8D9F FOREIGN KEY (material_invoice_id) REFERENCES material_invoice (id)');
        $this->addSql('ALTER TABLE construction ADD CONSTRAINT FK_DC91E26E5819624A FOREIGN KEY (lot_invoice_id) REFERENCES lot_invoice (id)');
        $this->addSql('ALTER TABLE sale ADD CONSTRAINT FK_E54BC005EC8B7ADE FOREIGN KEY (period_id) REFERENCES period (id)');
        $this->addSql('ALTER TABLE sale ADD CONSTRAINT FK_E54BC005176DFE85 FOREIGN KEY (apartment_id) REFERENCES apartment (id)');
        $this->addSql('ALTER TABLE sale ADD CONSTRAINT FK_E54BC0054D2A7E12 FOREIGN KEY (building_id) REFERENCES building (id)');
        $this->addSql('ALTER TABLE sale ADD CONSTRAINT FK_E54BC0059395C3F3 FOREIGN KEY (customer_id) REFERENCES customer (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE sale DROP FOREIGN KEY FK_E54BC005176DFE85');
        $this->addSql('ALTER TABLE sale DROP FOREIGN KEY FK_E54BC0054D2A7E12');
        $this->addSql('ALTER TABLE sale DROP FOREIGN KEY FK_E54BC0059395C3F3');
        $this->addSql('ALTER TABLE construction DROP FOREIGN KEY FK_DC91E26E5819624A');
        $this->addSql('ALTER TABLE construction DROP FOREIGN KEY FK_DC91E26E89BB8D9F');
        $this->addSql('ALTER TABLE sale DROP FOREIGN KEY FK_E54BC005EC8B7ADE');
        $this->addSql('ALTER TABLE construction DROP FOREIGN KEY FK_DC91E26E166D1F9C');
        $this->addSql('DROP TABLE apartment');
        $this->addSql('DROP TABLE building');
        $this->addSql('DROP TABLE construction');
        $this->addSql('DROP TABLE customer');
        $this->addSql('DROP TABLE lot_invoice');
        $this->addSql('DROP TABLE material_invoice');
        $this->addSql('DROP TABLE period');
        $this->addSql('DROP TABLE project');
        $this->addSql('DROP TABLE sale');
        $this->addSql('DROP TABLE messenger_messages');
    }
}
